FROM rocker/r-ver

ENV DEBIAN_FRONTEND noninteractive

RUN mkdir -p ~/TMP

RUN apt-get update
RUN apt-get install -y libxml2-dev libpng-dev libstdc++6 liblzma-dev
RUN apt-get install -y libtcl8.6 libtk8.6 libbz2-dev libclang-dev
RUN apt-get install -y libcurl4-openssl-dev libssl-dev libgit2-dev

RUN Rscript -e "install.packages('remotes',repos='https://cloud.r-project.org')"
RUN Rscript -e "remotes::install_github('s-u/unixtools')"

## packages needed for basic shiny functionality
RUN Rscript -e "library('unixtools'); set.tempdir('~/TMP');print(paste('TMPDIR is : ',  tempdir()));install.packages('devtools',repos='https://cloud.r-project.org', Ncpus = 6)"
RUN Rscript -e "library('unixtools'); set.tempdir('~/TMP');install.packages(c('shiny','shinydashboard', 'shinytest', 'shinythemes', 'shinyjs','ggplot2','dendextend','seriation','png', 'DT', 'shinycssloaders', 'data.table', 'promises','tidyr','dplyr','purrr','httpuv','mime','shinyWidgets','glue','rlang','shinyalert', 'feather','BiocManager','markdown'),repos='https://cloud.r-project.org', Ncpus = 6)"
RUN Rscript -e "library('unixtools'); set.tempdir('~/TMP');shinytest::installDependencies()"

# bioconductor packages
RUN Rscript -e "library('unixtools'); set.tempdir('~/TMP');library(BiocManager);BiocManager::install(c('DESeq2', 'rtracklayer','hexbin','Mfuzz','GenomicFeatures','edgeR'))"

# github packages
RUN Rscript -e "library('unixtools'); set.tempdir('~/TMP');devtools::install_github('ropensci/plotly')"
RUN Rscript -e "library('unixtools'); set.tempdir('~/TMP');devtools::install_github('talgalili/heatmaply')"
RUN Rscript -e "library('unixtools'); set.tempdir('~/TMP');devtools::install_github('rstudio/d3heatmap')"

# install the shiny application
RUN Rscript -e "library('unixtools'); set.tempdir('~/TMP');remotes::install_gitlab('avelt/great', dependencies='Imports', upgrade='never')"

# set host & port
COPY Rprofile.site /usr/lib/R/etc/
EXPOSE 3838

CMD ["R", "-e great::shiny_greatshinyapp()"]
