# How to use GREAT (GRape Expression ATlas) ?

## Access the GREAT shiny app via our website

Make a request for username and password at contact-GREAT-colmar@inrae.fr

Then, go to the GREAT website, use your id and password and access to GREAT application : https://great.colmar.inrae.fr/

## Second possibility : Install the GREAT shiny app as an R package (For Linux & Mac only!) : 

/!\ Install XQuartz on Mac before launching GREAT app, because GREAT app needs X11 /!\

R version >= 3.5.0

### Install following packages

install.packages("devtools")

if (!requireNamespace("BiocManager", quietly = TRUE))
install.packages("BiocManager")

BiocManager::install("edgeR")

BiocManager::install("DESeq2")

devtools::install_github("rstudio/d3heatmap")

BiocManager::install("GenomicFeatures")

BiocManager::install("Mfuzz")

devtools::install_gitlab("avelt/great")

### Launch the GREAT shiny app :

library("great")

#### there is a function to launch GREAT automatically on port 3838 of your computer

great::shiny_greatshinyapp()

#### if you want to launch it on an other port (if 3838 is taken), you have to use the runApp() function of the Shiny package

shiny::runApp("~/R/x86_64-pc-linux-gnu-library/3.5/great/greatshinyapp")

This command check and install all dependencies packages of GREAT, so the first run can be long depending on the number of packages to install and you internet connexion. 

In all cases, a web browser opens and when the GREAT application appears (not the white screen but buttons, blues borders etc), you can use GREAT app. At the second run, it is very more fast because all the packages were installed.
