comparison	nb genes up-regulated	nb genes down-regulated
1	AskoErysiphe_12h_vs_Erysiphe_24h	1761	2150
2	AskoErysiphe_12h_vs_Erysiphe_3d	5480	4871
3	AskoErysiphe_12h_vs_Erysiphe_6d	4943	4489
4	AskoErysiphe_24h_vs_Erysiphe_3d	4045	3069
5	AskoErysiphe_24h_vs_Erysiphe_6d	3538	2616
6	AskoErysiphe_3d_vs_Erysiphe_6d	1262	1384
