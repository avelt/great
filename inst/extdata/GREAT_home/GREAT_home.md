<img src="images/logo.png" alt="drawing" width="150"/>

<h1><span style="color:#2b93e8">Welcome to GREAT application !</span></h1>

GREAT (GRape Expression ATlas) allows you to analyse and visualize all public RNA-seq data from around 80 *Vitis vinifera* species. 

With GREAT, you can explore gene expression in about 2000 public RNA-seq samples with interactive heatmaps or expression tables. 

Also, this application performs some statistical analyses, like genes clustering or differential gene expression analysis !

**<span style="color:#2b93e8">Contacts :</span>**

For any question, contact us : contact-GREAT-colmar@inrae.fr

Amandine Velt, Lauriane Renault & Camille Rustenholz.

<a name="quick"></a>
<h2><span style="color:#2b93e8">Quick tour of the application</span></h2>

<a name="database"></a>
<h3><span style="color:#2b93e8">GREAT database</span></h3>

All the information of public RNA-seq data from Vitis vinifera is contained in a database which is available on the **"GREAT database"** tab.

<a name="quality"></a>
<h3><span style="color:#2b93e8">Quality control table</span></h3>

This tab shows a chart containing all the quality statistics of the GREAT analyses.

<a name="visualization"></a>
<h3><span style="color:#2b93e8">Gene expression visualization</span></h3>

Here, you can visualize gene's expression (RPKM) on an interactive heatmap. Samples can be filtered based on some criteria (organ, variety, ...) and the genes too, with a gene list or with chromosomal coordinates.

<a name="table"></a>
<h3><span style="color:#2b93e8">Gene expression table</span></h3>

In this tab, you can create a chart with the gene expression values (RPKM) according to the chosen filters, which are the same as for the "Gene expression visualization" tab. Your table can be downloaded.

<a name="clustering"></a>
<h3><span style="color:#2b93e8">Gene expression clustering</span></h3>

For some bioprojects (some authors), soft clustering of gene expression data analysis is available. You can view the results in this tab.

<a name="differential"></a>
<h3><span style="color:#2b93e8">Differential gene expression analysis</span></h3>

For some bioprojects (some authors), differential gene expression analysis is available. You can view the results in this tab.

<a name="wordclouds"></a>
<h2><span style="color:#2b93e8">Some wordclouds from GREAT database</span></h2>

<a name="organ"></a>
<h3><span style="color:#2b93e8">Diversity of organs in GREAT</span></h3>
<img src="images/Organ_wordcloud.png" alt="drawing" width="200"/>

<a name="variety"></a>
<h3><span style="color:#2b93e8">Diversity of varieties in GREAT</span></h3>
<img src="images/Variety_wordcloud.png" alt="drawing" width="400"/>

<a name="tissue"></a>
<h3><span style="color:#2b93e8">Diversity of tissues in GREAT</span></h3>
<img src="images/Tissue_wordcloud.png" alt="drawing" width="300"/> 

<a name="shortstage"></a>
<h3><span style="color:#2b93e8">Diversity of development stage in GREAT</span></h3>
<img src="images/Short_stage_wordcloud.png" alt="drawing" width="500"/>

<a name="treatment"></a>
<h3><span style="color:#2b93e8">Diversity of treatment in GREAT</span></h3>
<img src="images/Treatment_nature_wordcloud.png" alt="drawing" width="500"/>
