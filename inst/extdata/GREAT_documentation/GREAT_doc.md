<a name="introduction"></a>

<h2><span style="color:#2b93e8">Introduction</span></h2>

GREAT (GRape Expression ATlas) is an RNA-seq data analysis workflow
developped with
<a href="https://academic.oup.com/bioinformatics/article/28/19/2520/290322" target="_blank">Snakemake</a>
and a web application developped with <a href="https://shiny.rstudio.com/" target="_blank">R Shiny</a>, which allows to analyse and
visualize public RNA-seq data from *Vitis vinifera* species. It allows
to explore gene expression in about 2000 public RNA-seq samples with
interactive heatmaps or expression tables. Also, this application
performs some statistical analyses, like genes clustering with
<a href="https://www.bioconductor.org/packages/release/bioc/html/Mfuzz.html" target="_blank">MFuzz</a>
or differential gene expression analysis with
<a href="https://github.com/askomics/askoR" target="_blank">askoR</a> (edgeR).

<img src="images/logo.png" alt="drawing" width="150"/>

**<span style="color:#2b93e8">Technical contacts :</span>**

For the public RNA-seq samples database :
<a href="mailto:lauriane.renault@inrae.fr" class="email">lauriane.renault@inrae.fr</a>

For the RNA-seq data analysis workflow and the web application :
<a href="mailto:amandine.velt@inrae.fr" class="email">amandine.velt@inrae.fr</a>

**<span style="color:#2b93e8">Scientific contact :</span>**

<a href="mailto:camille.rustenholz@inrae.fr" class="email">camille.rustenholz@inrae.fr</a>

<a name="source"></a>

<h2><span style="color:#2b93e8">Source</span></h2>

GREAT RNA-seq analysis workflow :
<a href="https://gitlab.com/avelt/great_rnaseq_workflow" target="_blank">source</a>

GREAT R Shiny web application (R package) :
<a href="https://gitlab.com/avelt/great" target="_blank">source</a>

<a name="database"></a>

<h2><span style="color:#2b93e8">Public *Vitis vinifera* RNA-seq samples database</span></h2>

Data and informations about these datasets come from the
<a href="https://www.ncbi.nlm.nih.gov/sra/docs/" target="_blank">SRA</a> (Sequence Read
Archive, NCBI), the <a href="https://www.ebi.ac.uk/ena" target="_blank">ENA</a> (European
Nucleotide Archive, EBI) and the
<a href="https://www.ncbi.nlm.nih.gov/geo/" target="_blank">GEO</a> (Gene Expression Omnibus,
NCBI) databases and from publications, if existing, on
<a href="https://www.ncbi.nlm.nih.gov/pubmed/" target="_blank">pubmed</a>.

Before developping GREAT workflow and web application, we created the
GREAT database. We have identified and keep in a flat file all
informations about all relevant RNA-seq public data from *Vitis
vinifera* species. For that, we searched all the papers about RNA-seq on
*Vitis vinifera*, we recovered all the project IDs and all the RNA-seq
samples of all these papers. We created a flat file (our database) with
one line per sample and a full description of this sample.

The information is contained in this database are available on the **<span style="color:#2b93e8">GREAT
database</span>** tab of the GREAT R Shiny web application.

By clicking on **<span style="color:#2b93e8">GREAT database</span>** tab, you can access to the samples
information table. For each column of this table, a filter is available, 
in order to be able to filter the samples by author name, variety, treatment, etc ...

<img src="images/Image2.png" alt="drawing" width="1400"/>

The information available for each sample is :

-   **<span style="color:#2b93e8">BioProject</span>** : A BioProject is a collection of biological data
    related to a single initiative originating from a single
    organization or from a consortium. A BioProject record provides
    users a single place to find links to the diverse data generated for
    that project. In our case, this BioProject ID allows to link all
    RNAseq samples of a same experiment.
-   **<span style="color:#2b93e8">First\_author</span>** : The first author of the paper linked to the
    BioProject, if exists. Else, this is the name of the person who put
    the data on the public database.
-   **<span style="color:#2b93e8">Paper</span>** : A link to the scientific paper of the BioProject, if
    exists.
-   **<span style="color:#2b93e8">File\_name</span>** : SRR (or ERR, or DRR) is the SRA accession of a
    sample. There are several SRR ID linked to one BioProject ID. Each
    SRR/ERR/DRR is a sample of the BioProject.
-   **<span style="color:#2b93e8">Sequencing\_platform</span>** : Usually Illumina platform for RNA-seq
    data.
-   **<span style="color:#2b93e8">PE\_SE</span>** : If sample is paired-end (PE) or single-end (SE)
-   **<span style="color:#2b93e8">Read\_length\_bp</span>** : The read length for the sample
-   **<span style="color:#2b93e8">Protocol\_library</span>** : The protocol used to prepare the RNA-seq
    library. Usually PolyA if there was a polyA selection. Unspecified,
    if we didn’t find the protocol used in the paper. Finally, totalRNA
    if there was a ribosomal depletion rather than a polyA selection.
-   **<span style="color:#2b93e8">Stranded</span>** : if the RNA-seq protocol is stranded (“reverse” value,
    for dUTP protocole) or not (“no” value in the table)
-   **<span style="color:#2b93e8">Species</span>** : Always *Vitis vinifera*, it is the only species
    studied in this web application.
-   **<span style="color:#2b93e8">Variety</span>** : Variety of the sample (Tannat, Riesling, Pinot noir …)
-   **<span style="color:#2b93e8">Organ</span>** : Organ where the RNA was extracted (Berries, Leaves,
    Shoot, … )
-   **<span style="color:#2b93e8">Tissue</span>** : Tissue where the RNA was extracted (Whole, Skin, … )
-   **<span style="color:#2b93e8">Stage</span>** : The stage of the RNA-seq sample (5 weeks post-flowering,
    68 days after anthesis, … )
-   **<span style="color:#2b93e8">Added\_date</span>** : The date where the RNA-seq sample was added in the
    database
-   **<span style="color:#2b93e8">Treatment</span>** : If RNA-seq sample had a treatment or not (Yes,
    Control, -, No)
-   **<span style="color:#2b93e8">Treatment\_type</span>** : If yes, treatment type (Biotic, Abiotic)
-   **<span style="color:#2b93e8">Treatment\_nature</span>** : Treatment nature (Wound, Water deficit,
    Plasmopara viticola, …)
-   **<span style="color:#2b93e8">Treatment\_detail</span>** : Treament details (24h after Plasmopara
    viticola inoculation, 3 hours post-inoculation, …)
-   **<span style="color:#2b93e8">Comments</span>** : Some comments on the sample, if necessary
-   **<span style="color:#2b93e8">Complete\_Bioproject</span>** : if the QC of all samples of the
    Bioproject were validated (yes) or not (no = incomplete Bioproject,
    some samples are missing because of their low quality)

<a name="pipeline"></a>

<h2><span style="color:#2b93e8">GREAT : RNA-seq data analysis pipeline description</span></h2>

<h3><span style="color:#2b93e8">GREAT pipeline outputs</span></h3>

The files used by this Rshiny web application was generated with the
GREAT RNA-seq data analysis pipeline, developed with Snakemake.

The RNA-seq pipeline is available on GitHub at the following adress :
<a href="https://gitlab.com/avelt/great" target="_blank">RNA-seq pipeline</a>

This pipeline performs data QC with fastQC from SRA or fastq files, alignement
on the reference genome with STAR from SRA or fastq files, alignements QC and
parsing, gene expression counts with featureCounts and gene expression
counts normalization between all samples with DESeq2. At the end of the
pipeline, we have two files used by the web application :

-   **<span style="color:#2b93e8">all\_samples\_statistics.txt</span>** : statistics available on all
    samples (number of reads, number of reads mapped after parsing, …).
    This table is available via the navigator bar by clicking on
    “Quality control table”
-   **<span style="color:#2b93e8">normalized\_counts\_and\_annotation\_all\_genes\_CRIBI.rds</span>** :
    this table contains the raw read counts, the normalized counts
    (library size normalization) and the RPKM (gene size normalization)
    for each gene in each sample, for the VCost.v3 annotation. This
    table is available via the navigator bar by clicking on Gene
    expression table. This table is also used in “Gene expression visualization”
    to create the interactive heatmap of gene expression.

**Every 6 months, we look at the new RNA-seq papers published on Vitis
vinifera, we had new samples in our database, we recover these samples
and we launch our RNA-seq data analysis pipeline. New RNA-seq data are
automatically added to existing analyzed data and the web application is updated
with these new data.**

<h3><span style="color:#2b93e8">Pipeline steps</span></h3>

All the steps are run on the samples separately, except the
GREAT\_report step that will merge all the samples to get the final
files for the GREAT RShiny application.

<h4><span style="color:#2b93e8">sra_to_fastq</span></h4>

If the input file is in SRA format, it is converted in FASTQ file, which
is the common format for raw reads. This conversion is performed with SRA
Toolkit.

<h4><span style="color:#2b93e8">fastqc</span></h4>

fastQC is launched on the fastq file in order to generate quality
graphics and stats to check the sample quality.

<h4><span style="color:#2b93e8">star_map</span></h4>

Raw reads are aligned on 12X.2 assembly (PN40024) (with help of the VCost.v3 structural annotation) with STAR.

Options used :

-   **<span style="color:#2b93e8">outMultimapperOrder Random</span>** : by default the primary alignment is not
    selected randomly. In the case of multiple alignment locations, with
    the same alignment score, STAR randomly select one alignment as the
    best alignment, with this option.

-   **<span style="color:#2b93e8">outFilterMismatchNmax 999</span>** : because we want to consider mismatch in
    function of the reads length, not as a fixed number. So I wrote 999
    to not filter the alignments on this option (by default, at 10
    mismatchs the alignment is removed).

-   **<span style="color:#2b93e8">outSAMprimaryFlag OneBestScore</span>** : we want to keep the best alignment
    (primary alignment), so we tell to STAR to flag this best alignment,
    in order to recover it during the sam\_parsing step.

-   **<span style="color:#2b93e8">outSAMattributes All</span>** : allows to write all the SAM attributes in the
    bam file, in order to have the NM tag in the bam file and use it to
    filter with the edit distance parameter.

<h4><span style="color:#2b93e8">sam_parsing</span></h4>

Here, we filter the aligment file (SAM file)

Two filtering steps :

Keep the primary alignment of each read (or read pair) only.

Remove alignment with large edit distance (we keep only alignments with
edit distance &lt;= edit distance parameter). Edit distance parameter is
calculated with the read length to have an alignement of 95% identity
(because we work only on samples from Vitis vinifera species and that
the reference genome is from Vitis vinifera species). For example, if a
read have a length of 95 bp, edit distance equals to 5. (95\*95/100 =
90.25, we can’t keep 4.75 so we round up).

<h4><span style="color:#2b93e8">featureCounts_CRIBI_annotations</span></h4>

With the filtered alignements, we can now count number of reads per
gene, with the VCost.v3 structural annotation file, by using featureCounts
tool.

We keep only the reads which match one gene (if they match several genes
(overlapping genes), they are unassigned and considered as ambiguous).
And thanks to the previous filter, we count only the read with a unique
location on the genome (primary alignments).

<h4><span style="color:#2b93e8">stats_alignement_table</span></h4>

This step allows to recover some statistics on the analyzed sample
(number of reads, percent of reads mapped, percent of reads assigned to
a gene, etc)

<h4><span style="color:#2b93e8">GREAT_report</span></h4>

GREAT report allows to create the final files, used by the GREAT Rshiny
application. It merges the results of all sample in a common table.

-   **<span style="color:#2b93e8">statistics table</span>** : A table contaning all the statistics (number
    of reads, number of reads aligned, number of genes counted, .)

-   **<span style="color:#2b93e8">normalized counts table (RPKN)</span>** : A table with raw read counts,
    counts normalized by DESeq2 and RPKN. This table is used to plot the
    heatmap, to show the counts table and to perform MFuzz and askoR
    analyses. Raw read counts are the raw counts determined by
    featureCounts. Read counts normalized by DESeq2 is a library size
    normalization. So, the goal is to put all samples to a same number
    of total reads (a same sequencing depth) to be comparable. Finally,
    these normalized counts are divided by gene length (in kb) in order to
    compare genes between them. Indeed, genes have different lengths,
    and more a gene is big, more it has reads. So, by dividing the
    counts by gene length, this problem disappear.

<a name="reference"></a>

<h2><span style="color:#2b93e8">Reference files used</span></h2>

<h3><span style="color:#2b93e8">12X.2 assembly (PN40024)</span></h3>

12X.2 version of the grapevine reference genome sequence from The
French-Italian Public Consortium for the Grapevine Genome
Characterization (PN40024). The sequence of the contigs and scaffolds
are unchanged compared to the 12X.0 version. The chromosome assembly has
been improved.

Reference paper :

Canaguier A, Grimplet J, Scalabrin S, Di Gaspero G, Mohellibi N, Choisne
N, Rombauts S, Ruztenholtz C, Morgante M, Quesneville H, Adam-Blondon
A-F (2017) A new version of the grapevine reference genome assembly
(12X.v2) and of its annotation (VCost.v3). DOI:
10.15454/1.5009072354498936E12

<h3><span style="color:#2b93e8">VCost.v3 structural annotation</span></h3>

Reference gene annotation of the 12X.2 assembly containing
42,413 genes.

Reference paper :

Canaguier A, Grimplet J, Scalabrin S, Di Gaspero G, Mohellibi N, Choisne
N, Rombauts S, Ruztenholtz C, Morgante M, Quesneville H, Adam-Blondon
A-F (2017) A new version of the grapevine reference genome assembly
(12X.v2) and of its annotation (VCost.v3). DOI:
10.15454/1.5009072354498936E12

<a name="shiny"></a>

<h2><span style="color:#2b93e8">GREAT RShiny application : navigator bar description</span></h2>

<h3><span style="color:#2b93e8">GREAT database</span></h3>

This table allows to see the information about all samples available on
GREAT shiny application. For each column of this table, a filter is available, 
in order to be able to filter the samples by author name, variety, treatment, etc ...

<h3><span style="color:#2b93e8">Quality control table</span></h3>

This table contains all the quality statistics of the GREAT analysis.
For each sample, there is the number of reads, the average reads length,
the number of reads mapped and used for counting, and the number of
reads assigned to a gene. We consider that if there is less than 60%/70%  of
reads aligned or less than 60%/70% of reads assigned to a gene, there is
maybe a problem with the sample. So, you have to take the analysis
results of this sample with caution. Also, you have to check the total
number of reads, if there is small you have to take the analysis
results of this sample with caution.

<h3><span style="color:#2b93e8">Gene expression visualization</span></h3>

<h4><span style="color:#2b93e8">Choose your samples</span></h4>

First, you have to choose the filters. For that, select “Treatment type”. If
you are not interested in a particular treatment, select “Select All”. Depending on the filter chosen, the other
filters update. Then, you can choose the “Treatment nature”
among these “Treatment type”, then the “Variety”, the “Organ” of
grapevine and finally the “Bioproject”.

<h4><span style="color:#2b93e8">Choose your genes</span></h4>

There are two possibilities to choose your genes of interest. Either you
give a gene list, by default 8 genes are displayed.

Either you change the gene selection method and choose “Chromosome
coordinates”. Here you give the chromosomal region that interests you.
Example: chr1 / 120000 / 220000 / \* -&gt; here, all the genes included
in the range 120000-220000 of chromosome 1, on the two strands, will be
displayed.

<h4><span style="color:#2b93e8">Choose your counts</span></h4>

By default the RPKM are displayed but in the case where the RPKM is very
high in one condition and overwrites the other gene expression, you can
choose the log(RPKM).

<h4><span style="color:#2b93e8">Start the analysis</span></h4>

Once all the parameters have been completed, click on the “Launch
Analysis” button.

<h4><span style="color:#2b93e8">The outputs</span></h4>

<h5><span style="color:#2b93e8">Heatmap on genes expression (RPKM or log(RPKM))</span></h5>

Here, you can see the expression of your selected genes in all the
samples selected.

<h5><span style="color:#2b93e8">Information table</span></h5>

A subset of the GREAT database with the selected samples.

<h3><span style="color:#2b93e8">Gene expression table</span></h3>

In this section, you can perform the same filters than in “Gene
expression visualization” and a tables with RPKM values for these samples will appear.
Also, at the end of the web page, there is a subset of the GREAT database with the selected samples.

<h3><span style="color:#2b93e8">Gene expression clustering</span></h3>

For some Bioproject (some author), soft clustering of gene expression
data analysis is available. This analysis was generated with MFuzz :
<a href="https://www.bioconductor.org/packages/release/bioc/vignettes/Mfuzz/inst/doc/Mfuzz.pdf" target="_blank">MFuzz documentation</a>

<h4><span style="color:#2b93e8">Select an analysis</span></h4>

You can select an author/bioproject, the number of clusters to generate and the
membership cutoff (see MFuzz documentation for explanation). Then click
on “Launch analysis” button.

<h4><span style="color:#2b93e8">The outputs</span></h4>

There is one graph per cluster, which allows to see the
trends of gene expression between the conditions. Then, there is a
result table per cluster, with the genes belonging to this cluster and
their membership. Finally, there is a subset of the GREAT database with the selected samples.

<h3><span style="color:#2b93e8">Differential gene expression analysis</span></h3>

Thanks to a tool called askoR, a differential expression analysis was
carried out for some experiments (experiments with good quality samples and presence of replicates) :
<a href="https://github.com/askomics/askoR" target="_blank">askoR</a>

<h4><span style="color:#2b93e8">Select the analysis</span></h4>

You can select an author/bioproject and click on “Launch analysis” button to show
the gene differential expression analysis.

<h4><span style="color:#2b93e8">The outputs</span></h4>

The first table summarizes all the comparisons performed by askoR, with the number of differentially 
expressed genes. For each comparison, the analysis table can be downloaded.

Finally, there is a subset of the GREAT database with the selected samples.
