.. GREAT documentation master file, created by
   sphinx-quickstart on Wed Sep  4 11:18:15 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to GREAT's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Web application
=================================
A web application accessible by a web adress is under construction. For now, the GREAT shiny app is available as an R package.

Install the GREAT web application
=================================

**Warning** : Install XQuartz on Mac before launching GREAT app, because GREAT app needs X11.

**Requirements** : R version >= 3.5.0

**Installation of required R packages** :

.. code-block:: python
    :linenos:

    install.packages("shiny")
    install.packages("devtools")
    install.packages("https://gitlab.com/avelt/great/raw/master/great_0.1.0.tar.gz", repos = NULL, type="source")

Launch Shiny app
=================================

.. code-block:: python
    :linenos:

    library("great")
    great::shiny_greatshinyapp()

Get the code
=================================

The `source <https://gitlab.com/avelt/great_rnaseq_workflow>`_ code of the GREAT's RNA-seq pipeline is available on Gitlab. The `source <https://gitlab.com/avelt/great>`_  code for the Shiny R package is available on Gitlab.

**Technical contact** : amandine.velt@inra.fr

**Scientific contact** : camille.rustenholz@inra.fr

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

.. Example to show code
.. ==================

.. .. code-block:: python
..    :linenos:

..    import antigravity

..    def main():
..        antigravity.fly()


.. Create table
.. ==================
.. .. csv-table:: Frozen Delights!
..    :header: "Treat", "Quantity", "Description"
..    :widths: 15, 10, 30

..    "Albatross", 2.99, "On a stick!"
..    "Popcorn", 1.99, "Straight from the oven"

.. Can be done from file too.