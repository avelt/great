#' GREAT shiny application
#'
#' This function allows to launch the GREAT shiny app and open a web browser with the application available
#' @param - no param for this function
#' @export
#' @examples shiny_greatshinyapp()
# shiny_greatshinyapp <- function() { appDir <- system.file("greatshinyapp", package = "great")  ;Sys.setlocale('LC_ALL','C');shiny::runApp(appDir)}
run_app <- function() { appDir <- system.file("greatshinyapp", package = "great")  ;Sys.setlocale('LC_ALL','C');shiny::runApp(appDir, host='0.0.0.0', port=3838)}
